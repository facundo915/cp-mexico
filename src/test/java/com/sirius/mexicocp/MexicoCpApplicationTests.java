package com.sirius.mexicocp;

import com.sirius.mexicocp.dto.Location;
import com.sirius.mexicocp.service.InputPreprocessor;
import com.sirius.mexicocp.service.PostalCodeService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MexicoCpApplicationTests {
    @Autowired
    PostalCodeService postalCodeService;
    @Autowired
    InputPreprocessor inputPreprocessor;

    @Test
    public void contextLoads() {
    }

    @Test
    public void findByAsentamiento() {
        Optional<Integer> test = postalCodeService.findByAsentamiento("Fracc Alturas del Sur");
        Assert.assertEquals(80295, (long) test.get());
    }
    @Test
    public void findByLocation(){
        Location sanjuan = new Location();
        sanjuan.setRegion("ciudad de mexico");
        sanjuan.setMunicipality("iztapalapa");
        sanjuan.setColony("paraje san juan");
        postalCodeService.findByLocation(sanjuan);
    }

    @Test
    public void matches() throws IOException {
        Stream<String> lines = Files.lines(Paths.get("/home/facundo/Documents/test.csv"));
        //region	municipality	colony	postalcode
        AtomicInteger total = new AtomicInteger();
        AtomicInteger equal = new AtomicInteger();
        AtomicInteger corrected = new AtomicInteger();
        AtomicInteger notFound = new AtomicInteger();
        AtomicInteger notAssigned = new AtomicInteger();
        AtomicInteger notAssignedAndFound = new AtomicInteger();
        AtomicInteger notAssignedAndNotFound = new AtomicInteger();
        StringBuilder sb = new StringBuilder();

        lines.skip(1).forEach(line -> {
            String[] split = line.split(";");
            Location l = new Location();
            if (split.length == 0)
                return;
            total.getAndIncrement();
            System.out.print("\r");
            System.out.print("Total: " + total.toString());

            l.setRegion(split[0]);
            l.setMunicipality(split[1]);//WARNING: Inverted
            if (split.length > 2)
                l.setColony(split[2]);

            l = inputPreprocessor.process(l);

            Optional<Integer> found = postalCodeService.findByLocation(l);

            Integer codeTanzi = null;
            if (split.length > 3) {
                codeTanzi = Integer.parseInt(split[3]);
                if (found.isPresent() && codeTanzi.equals(found.get()))
                    equal.getAndIncrement();
                else {
                    if (found.isPresent()) {
                        corrected.getAndIncrement();
                    } else {
                        notFound.getAndIncrement();
                        // System.out.println("[NOT FOUND] ");
                        //System.out.println("[LOCATION] Region: " + l.getRegion() + " Municipality: " + l.getMunicipality() + " Colony: " + l.getColony() );
                    }
                    //  System.out.println("[CHANGED] new: " + codeFound + " old: " + codeTanzi);
                    //  System.out.println("[LOCATION] Region: " + l.getRegion() + " Municipality: " + l.getMunicipality() + " Colony: " + l.getColony() );
                }
            } else {
                notAssigned.getAndIncrement();
                if (found.isPresent())
                    notAssignedAndFound.getAndIncrement();
                else
                    notAssignedAndNotFound.getAndIncrement();
            }
            sb.append(l.getRegion()).append(";").append(l.getMunicipality()).append(";").append(l.getColony()).append(";").append(codeTanzi == null ? "" : codeTanzi).append(";").append(found.isPresent() ? found.get() : "");
            sb.append('\n');
        });

        File out = new File("/home/facundo/mexico-results.csv");
        FileOutputStream fileOutputStream = new FileOutputStream(out);
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(fileOutputStream));
        writer.write(sb.toString());

        System.out.println("Total : " + total);
        System.out.println("Equal : " + equal);
        System.out.println("Corrected: " + corrected);
        System.out.println("Not found codes: " + notFound);
        System.out.println("Not assigned: " + notAssigned);
        System.out.println("Not assigned and found: " + notAssignedAndFound);
        System.out.println("Not assigned and not found" + notAssignedAndNotFound);
    }

}
