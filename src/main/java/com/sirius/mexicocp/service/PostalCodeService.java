package com.sirius.mexicocp.service;

import com.sirius.mexicocp.dto.Location;
import com.sirius.mexicocp.model.Asentamiento;
import com.sirius.mexicocp.model.Estado;
import com.sirius.mexicocp.model.Municipio;
import com.sirius.mexicocp.model.TipoAsentamiento;
import com.sirius.mexicocp.repository.AsentamientoRepository;
import com.sirius.mexicocp.repository.EstadoRepository;
import com.sirius.mexicocp.repository.MunicipioRepository;
import com.sirius.mexicocp.repository.TipoAsentamientoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.text.similarity.*;

import javax.transaction.Transactional;
import java.text.Normalizer;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class PostalCodeService {
    @Autowired
    AsentamientoRepository asentamientoRepository;
    @Autowired
    MunicipioRepository municipioRepository;
    @Autowired
    EstadoRepository estadoRepository;
    @Autowired
    TipoAsentamientoRepository tipoAsentamientoRepository;

    public Optional<Integer> findByAsentamiento(String asentamiento) {
        asentamiento = normalize(asentamiento);
        List<Asentamiento> asentamientos = asentamientoRepository.findByNombreNormal(asentamiento);

        if (asentamientos.isEmpty())
            asentamientos = asentamientoRepository.findByNombreNormalContaining(asentamiento);

        if (asentamientos.isEmpty()) {
            String droppingFront = asentamiento;
            List<Asentamiento> first = new ArrayList<>();
            String droppingBack = asentamiento;
            List<Asentamiento> second = new ArrayList<>();

            while (first.isEmpty() && !droppingFront.equals("")) {
                droppingFront = dropFirstWord(droppingFront);
                first = asentamientoRepository.findByNombreNormal(droppingFront);
                if (first.isEmpty())
                    first = asentamientoRepository.findByNombreNormalContaining(droppingFront);
            }
            while (second.isEmpty() && !droppingBack.equals("")) {
                droppingBack = dropLastWord(droppingBack);
                second = asentamientoRepository.findByNombreNormal(droppingBack);
                if (second.isEmpty())
                    second = asentamientoRepository.findByNombreNormalContaining(droppingBack);
            }
            if (first.isEmpty())
                asentamientos = second;
            else if (second.isEmpty())
                asentamientos = first;
            else if (second.size() < first.size())
                asentamientos = second;
            else
                asentamientos = first;
        }
        if(asentamientos.size() != 1)
            return Optional.empty();

        return Optional.ofNullable(asentamientos.get(0).getCodigo());
    }


    private Optional<Integer> findByAsentamientoAndMunicipio(String asentamiento, Municipio municipio) {
        asentamiento = normalize(asentamiento);

        Set<Asentamiento> asentamientos = municipio.getAsentamientos();

        String findType = asentamiento;
        Optional<TipoAsentamiento> tipoAsentamiento = Optional.empty();

        while (!tipoAsentamiento.isPresent() && !findType.equals("")) {
            findType = dropLastWord(findType);
            tipoAsentamiento = queryTipoAsentamiento(findType);
        }

        if (tipoAsentamiento.isPresent()) {
            Optional<TipoAsentamiento> finalTipoAsentamiento = tipoAsentamiento;
            asentamientos = asentamientos.stream().filter(a -> a.getTipoAsentamiento().equals(finalTipoAsentamiento.get())).collect(Collectors.toSet());
            asentamiento = asentamiento.replace(findType,"");
        }

        for(Asentamiento x : asentamientos){
            if(x.getNombreNormal().equals(asentamiento))
                return Optional.of(x.getCodigo());
        }

        return query(asentamientos,asentamiento);
    }

    public Optional<Integer> findByAsentamientoAndMunicipio(String asentamiento, String municipio) {
        municipio = normalize(municipio);
        List<Municipio> municipios = municipioRepository.findByNombreNormal(municipio);
        if (municipios.isEmpty())
            municipios = municipioRepository.findByNombreNormalContaining(municipio);
        municipio = dropFirstWord(municipio);

        if (municipios.isEmpty())
            municipios = municipioRepository.findByNombreNormal(municipio);
        if (municipios.isEmpty() && !municipio.equals(""))
            municipios = municipioRepository.findByNombreNormalContaining(municipio);
        if (municipios.isEmpty()) {
            return findByAsentamiento(asentamiento);
        }
        if(municipios.size() > 1)
            return Optional.empty();

        return findByAsentamientoAndMunicipio(asentamiento, municipios.get(0));
    }

    private Optional<Integer> findByAsentamientoAndMunicipioAndEstado(String asentamiento, String municipio, Estado estado) {
        municipio = normalize(municipio);

        Set<Municipio> municipios = estado.getMunicipios();

        for(Municipio x : municipios){
            if(x.getNombreNormal().equals(municipio))
                return findByAsentamientoAndMunicipio(asentamiento,x);
        }
        Optional<Municipio> municipioOptional = queryMunicipio(municipios,municipio);
        if(municipioOptional.isPresent())
            return findByAsentamientoAndMunicipio(asentamiento,municipioOptional.get());
        return findByAsentamiento(asentamiento);
    }

    /**
     * Searches postal codes for the location given
     *
     * @param asentamiento String
     * @param municipio    String
     * @param estado       String
     * @return All postal codes found for this location
     * <p>
     * This function only finds the state and then calls findByAsentamientoAndMunicipioAndEstado() with the state
     */
    public Optional<Integer> findByAsentamientoAndMunicipioAndEstado(String asentamiento, String municipio, String estado) {
        estado = normalize(estado);
        List<Estado> estados = estadoRepository.findByNombreNormal(estado);
        if (estados.isEmpty() && !estado.equals(""))
            estados = estadoRepository.findByNombreNormalContaining(estado);
        estado = dropFirstWord(estado);

        if (estados.isEmpty())
            estados = estadoRepository.findByNombreNormal(estado);
        if (estados.isEmpty() && !estado.equals(""))
            estados = estadoRepository.findByNombreNormalContaining(estado);
        if (estados.isEmpty())
            return findByAsentamientoAndMunicipio(asentamiento, municipio);
        return findByAsentamientoAndMunicipioAndEstado(asentamiento, municipio, estados.get(0));
    }

    //Protip: leer desde aca para arriba, los metodos se van haciendo mas especificos.
    public Optional<Integer> findByLocation(Location location) {
        if (location.getColony() != null) {
            if (location.getMunicipality() != null) {
                if (location.getRegion() != null)
                    return findByAsentamientoAndMunicipioAndEstado(location.getColony(), location.getMunicipality(), location.getRegion());
                return findByAsentamientoAndMunicipio(location.getColony(), location.getMunicipality());
            }
            return findByAsentamiento(location.getColony());
        }
        return Optional.empty();
    }


    //Remove tildes and lowerCase the string
    private String normalize(String s) {
        return Normalizer
                .normalize(s, Normalizer.Form.NFD)
                .replaceAll("[^\\p{ASCII}]", "")
                .toLowerCase()
                .trim();
    }

    private String dropFirstWord(String s) {
        return Arrays.stream(s.split(" ")).skip(1).collect(Collectors.joining(" "));
    }

    private String dropLastWord(String s) {
        String[] split = s.split(" ");
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < split.length - 1; i++) {
            sb.append(split[i]);
            if (!(i == (split.length - 2)))
                sb.append(" ");
        }
        return sb.toString();
    }

    private Optional<Integer> query(Set<Asentamiento> possibles, String query){
        FuzzyScore fz = new FuzzyScore(Locale.ENGLISH);
        Map<Asentamiento, Integer> scores = new HashMap<>();
        for (Asentamiento x : possibles) {
            if(x.getNombreNormal().length() == query.length())
                scores.put(x, fz.fuzzyScore(x.getNombreNormal(), query) + 4);
            else
                scores.put(x, fz.fuzzyScore(x.getNombreNormal(), query));
        }

        Optional<Map.Entry<Asentamiento, Integer>> max = scores.entrySet().stream().max(Comparator.comparingInt(Map.Entry::getValue));
        if (max.isPresent()) {

            //Passes worst score for 1 swap
            boolean acceptable = max.get().getValue() >= ((query.length()/2) -1) * 6 + 2;

            if (acceptable) {
                return Optional.of(max.get().getKey().getCodigo());
            }
        }
        return Optional.empty();
    }

    private Optional<Municipio> queryMunicipio(Set<Municipio> possibles, String query){
        FuzzyScore fz = new FuzzyScore(Locale.ENGLISH);
        Map<Municipio, Integer> scores = new HashMap<>();
        for (Municipio x : possibles) {
            if(x.getNombreNormal().length() == query.length())
                scores.put(x, fz.fuzzyScore(x.getNombreNormal(), query) + 4);
            else
                scores.put(x, fz.fuzzyScore(x.getNombreNormal(), query));
        }

        Optional<Map.Entry<Municipio, Integer>> max = scores.entrySet().stream().max(Comparator.comparingInt(Map.Entry::getValue));
        if (max.isPresent()) {

            //Passes worst score for 1 swap
            boolean acceptable = max.get().getValue() >= ((query.length()/2) -1) * 6 + 2;

            if (acceptable) {
                return Optional.of(max.get().getKey());
            }
        }
        return Optional.empty();
    }
    private Optional<TipoAsentamiento> queryTipoAsentamiento(String query) {

        //EXCEPTIONS
        if(query.equals("fracc")){
            return tipoAsentamientoRepository.findById(21);//Fraccionamiento
        }
        if(query.equals("unidad hab")){
            return tipoAsentamientoRepository.findById(31);//Unidad habitable
        }
        return Optional.empty();
    }

}


