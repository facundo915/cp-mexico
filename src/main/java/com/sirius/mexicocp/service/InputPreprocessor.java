package com.sirius.mexicocp.service;

import com.sirius.mexicocp.dto.Location;
import org.springframework.stereotype.Service;

@Service
public class InputPreprocessor {

    public String process(String str){
        if(str == null)
            return null;
        if(str.contains("1er")){
            str = str.replace("1er","1°");
        }
        if(str.contains("5to")){
            str = str.replace("5to","5°");
        }
        return str;
    }

    public Location process(Location location){
        Location temp = new Location();
        temp.setColony(process(location.getColony()));
        temp.setMunicipality(process(location.getMunicipality()));
        temp.setRegion(process(location.getRegion()));
        return temp;
    }
}
