package com.sirius.mexicocp.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Municipio {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private Integer codigo;
    private String nombre;
    private String nombreNormal;

    @OneToMany(
            mappedBy = "municipio",
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY
    )
    private Set<Asentamiento> asentamientos = new HashSet<>();

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "estado_id")
    private Estado estado;

    public Municipio(Integer codigo,String nombre,String nombreNormal, Estado estado){
        this.codigo = codigo;
        this.nombre = nombre;
        this.estado = estado;
        this.nombreNormal = nombreNormal;
    }

    public Municipio() {
    }

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (!(o instanceof Municipio )) return false;
        return id != null && id.equals(((Municipio) o).getId());
    }

    @Override
    public int hashCode() {
        return 960;
    }


    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Set<Asentamiento> getAsentamientos() {
        return asentamientos;
    }

    public void setAsentamientos(Set<Asentamiento> asentamientos) {
        this.asentamientos = asentamientos;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public String getNombreNormal() {
        return nombreNormal;
    }

    public void setNombreNormal(String nombreNormal) {
        this.nombreNormal = nombreNormal;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
