package com.sirius.mexicocp.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Ciudad {
    @Id
    private Integer codigo;
    private String nombre;
    private String nombreNormal;

    @OneToMany(
            mappedBy = "ciudad",
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY
    )
    private Set<Asentamiento> asentamientos = new HashSet<>();

    public Ciudad(Integer codigo, String nombre, String nombreNormal){
        this.codigo = codigo;
        this.nombre = nombre;
        this.nombreNormal = nombreNormal;
    }

    public Ciudad(){}

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (!(o instanceof Ciudad )) return false;
        return codigo != null && codigo.equals(((Ciudad) o).getCodigo());
    }

    @Override
    public int hashCode() {
        return 910;
    }


    public Set<Asentamiento> getAsentamientos() {
        return asentamientos;
    }

    public void setAsentamientos(Set<Asentamiento> asentamientos) {
        this.asentamientos = asentamientos;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreNormal() {
        return nombreNormal;
    }

    public void setNombreNormal(String nombreNormal) {
        this.nombreNormal = nombreNormal;
    }
}
