package com.sirius.mexicocp.controller;

import com.sirius.mexicocp.service.InputPreprocessor;
import com.sirius.mexicocp.dto.Location;
import com.sirius.mexicocp.service.PostalCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
public class PostalCodeController {

    @Autowired
    PostalCodeService postalCodeService;
    @Autowired
    InputPreprocessor inputPreprocessor;

    @PostMapping("/cp")
    public ResponseEntity<Integer> getCp(@RequestBody Location location){
        Optional<Integer> result = postalCodeService.findByLocation(inputPreprocessor.process(location));
        if(result.isPresent())
            return ResponseEntity.ok(result.get());
        return ResponseEntity.notFound().build();
    }
}
