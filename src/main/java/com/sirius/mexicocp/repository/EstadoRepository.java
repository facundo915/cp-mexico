package com.sirius.mexicocp.repository;

import com.sirius.mexicocp.model.Estado;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EstadoRepository extends JpaRepository<Estado, Integer> {
    List<Estado> findByNombreNormal(String nombreNormal);
    List<Estado> findByNombreNormalContaining(String nombreNormal);
}
