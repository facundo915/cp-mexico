package com.sirius.mexicocp.repository;

import com.sirius.mexicocp.model.Asentamiento;
import com.sirius.mexicocp.model.Ciudad;
import com.sirius.mexicocp.model.Estado;
import com.sirius.mexicocp.model.Municipio;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface AsentamientoRepository extends JpaRepository<Asentamiento,Integer> {
    Optional<Asentamiento> findAsentamientoByCodigoAndNombre(Integer codigo, String nombre);

    List<Asentamiento> findByNombreNormal(String nombre);
    List<Asentamiento> findByNombreNormalContaining(String nombre);
    List<Asentamiento> findByMunicipioAndNombreNormal(Municipio municipio, String nombreNormal);
    List<Asentamiento> findByMunicipioAndNombreNormalContaining(Municipio municipio, String nombreNormal);


}
