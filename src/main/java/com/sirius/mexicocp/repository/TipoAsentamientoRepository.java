package com.sirius.mexicocp.repository;

import com.sirius.mexicocp.model.TipoAsentamiento;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TipoAsentamientoRepository extends JpaRepository<TipoAsentamiento,Integer> {
    List<TipoAsentamiento> findByNombre(String find);

    List<TipoAsentamiento> findByNombreContaining(String find);
}
