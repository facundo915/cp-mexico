package com.sirius.mexicocp.repository;

import com.sirius.mexicocp.model.Asentamiento;
import com.sirius.mexicocp.model.Estado;
import com.sirius.mexicocp.model.Municipio;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MunicipioRepository extends JpaRepository<Municipio,Integer> {
    List<Municipio> findByNombreNormal(String nombre);
    List<Municipio> findByNombreNormalContaining(String nombre);
    List<Municipio> findByEstadoAndNombreNormal(Estado estado, String nombreNormal);
    List<Municipio> findByEstadoAndNombreNormalContaining(Estado estado, String nombreNormal);
}
